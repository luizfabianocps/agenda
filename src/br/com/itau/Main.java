package br.com.itau;

import br.com.itau.Agenda;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Agenda agenda = new Agenda();
        Pessoa pessoa = new Pessoa();

        EntradaDados.adicionarPessoaContato(agenda);

        agenda.exibeAgenda();

        if (EntradaDados.intensaoDeRemocao()){
            EntradaDados.deletarPessoaContato(agenda);
            agenda.exibeAgenda();
        }

    }
}
