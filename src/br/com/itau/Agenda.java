package br.com.itau;


import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;


public class Agenda {

    private List<Pessoa> agenda = new LinkedList<Pessoa>();

    public Agenda(){};


    public void adicionaContato(Pessoa pessoa){

        this.agenda.add(pessoa);
    }

    public void adicionarMuitosContatos(int quantidadeDePessoas){
        Scanner entrada = new Scanner(System.in);

        for (int i = 0; i < quantidadeDePessoas; i++){
            Pessoa novaPessoa = new Pessoa();
            System.out.println("Digite o nome:");
            novaPessoa.setNome(entrada.next());
            System.out.println("Digite o email:");
            novaPessoa.setEmail(entrada.next());
            System.out.println("Digite o número");
            novaPessoa.setNumero(entrada.next());
            this.adicionaContato(novaPessoa);
        }

    }

    public void deletarContatoPorEmail(String email){
        int i;
        //Executa o for enquanto essa afirmação for false, quando for true ele sai e remove
        for(i = 0; agenda.get(i).getEmail().equals(email); i++){

        }
        agenda.remove(i);
    }

    public void deletarPorTelefone(String telefone){
        int i;
        for(i = 0; agenda.get(i).getNumero().equals(telefone); i++){

        }
        agenda.remove(i);
    }


    public void exibeAgenda(){
        for (Pessoa pessoa: agenda) {
            System.out.println("Nome contato: " + pessoa.getNome());
        }
    }

    public List<Pessoa> getAgenda() {
        return agenda;
    }

    public void setAgenda(List<Pessoa> agenda) {
        this.agenda = agenda;
    }
}
