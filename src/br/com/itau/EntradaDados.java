package br.com.itau;

import com.sun.xml.internal.ws.client.ClientSchemaValidationTube;

import java.util.*;

public class EntradaDados {


    public static void  adicionarPessoaContato(Agenda agenda){
        System.out.println("Informe a quantidade de contatos a serem inseridos");
        agenda.adicionarMuitosContatos(scannerFix().nextInt());
    }

    public static boolean intensaoDeRemocao(){
        System.out.println("Gostaria de remover um contato? S/N");
        String resposta = scannerFix().next();
        if(resposta.equals("S") || resposta.equals("s")){
            return true;
        } else {
            return false;
        }

    }

    public static void deletarPessoaContato(Agenda agenda){
        System.out.println("Qual tipo de remoção quer fazer, por E-mail? S/N");
        String resposta = scannerFix().next();
        if(resposta.equals("S") || resposta.equals("s")){
            System.out.println("Informe o e-mail: ");
            String email = scannerFix().next();
            agenda.deletarContatoPorEmail(email);
        } else {
            System.out.println("Entendido, então por favor informe o número de telefone ");
            String telefone = scannerFix().next();
            agenda.deletarPorTelefone(telefone);
        }
    }

    //Exemplo de método que corrige bug de entrada de dados
    private static Scanner scannerFix() {

        return new Scanner(System.in);
    }

}
